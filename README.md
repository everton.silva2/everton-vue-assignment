# everton-vue3-assignment

## Note
Due the json-placeholder API implementation, it's not possible to edit a created comment.
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Run your unit tests
```
npm run test:unit
```
