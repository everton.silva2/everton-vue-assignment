export class CommentsService {
    defaultLimit = 5
    BASE_URL = `https://jsonplaceholder.typicode.com/comments`
    
    getAll() {
        return fetch(`${ this.BASE_URL }?_limit=${ this.defaultLimit }`)
            .then(response => response.json())
    }

    create(comment) {
        return fetch(
            this.BASE_URL,
            {
                method: 'POST',
                body: JSON.stringify(comment),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then(response => response.json())
    }

    edit(comment) {
        return fetch(
            `${ this.BASE_URL }/${ comment.id }`,
            {
                method: 'PUT',
                body: JSON.stringify(comment),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
        ).then(response => response.json())
    }

    delete(id) {
        return fetch(
            `${ this.BASE_URL }/${ id }`, 
            { 
                method: 'DELETE'
            },
        )
    }
}