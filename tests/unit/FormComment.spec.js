import { mount, shallowMount } from '@vue/test-utils'
import FormComment from '@/components/FormComment'

const commentForm = {
  id: '1',
  name: 'name',
  email: 'email',
  body: 'body'
}

const factory = (data = {}) => {
  return mount(FormComment, {
      data() {
          return {
              ...data,
              commentForm
          }
      }
  })
}

describe('FormComment.vue', () => {
  it('Should emit the add event when the form is submitted', async () => {
    const wrapper = factory()

    await wrapper.find('[data-test=btn-submit]').trigger('submit')

    expect(wrapper.emitted()['add-comment'][0][0]).toBe(commentForm)
  })

  it('Should emit the edit event when the form is submitted on edit state' , async () => {
    const wrapper = factory({
      isEditing: true,
    })

    await wrapper.find('[data-test=btn-submit]').trigger('submit')

    expect(wrapper.emitted()['edit-comment'][0][0]).toBe(commentForm)
  })

  it('Should display new comment button when isEditing is true', async () => {
    const wrapper = factory({ isEditing: true })

    expect(wrapper.find('[data-test=btn-toggle]').exists()).toBeTruthy()
  })

  it('Should not display new comment button when isEditing is false', async () => {
    const wrapper = factory()
    
    expect(wrapper.find('[data-test=btn-toggle]').exists()).toBeFalsy()
  })

  it('Should change the state of the form when new comment button is clicked', async () => {
    const wrapper = factory({ isEditing: true })

    const submitButton = wrapper.find('[data-test=btn-submit]')
    
    expect(submitButton.text()).toBe('Edit comment')

    await wrapper.find('[data-test=btn-toggle]').trigger('click')

    expect(submitButton.text()).toBe('Create comment')
  })

  it('Should fill the form and change state to edit when the function is called', async () => {
    const wrapper = factory()

    await wrapper.vm.updateForm(commentForm)

    expect(wrapper.find('[data-test=input-name]').element.value).toBe('name')
    expect(wrapper.find('[data-test=input-email]').element.value).toBe('email')
    expect(wrapper.find('[data-test=input-body]').element.value).toBe('body')
    expect(wrapper.find('[data-test=btn-submit]').text()).toBe('Edit comment')
  })

  it('Should not emit event when form validation fails', async () => {
    const wrapper = factory()
    wrapper.setData({
      commentForm: {
        name: 'Name',
        email: 'email',
        body: ''
      }
    })
    
    await wrapper.find('button[type=submit]').trigger('submit')

    expect(wrapper.emitted()).not.toHaveProperty('add-comment')
  })
})
