import { mount } from '@vue/test-utils'
import CommentList from '@/components/CommentList'
import CommentItem from '@/components/CommentItem'

const factory = (data = {}) => {
    return mount(CommentList, {
        propsData: {
            ...data,
        }
    })
}
const comments = [
    { id: 1, name: 'comment name 1', email: 'comment email 1', body: 'comment body 1' },
    { id: 2, name: 'comment name 2', email: 'comment email 2', body: 'comment body 2' }
]

describe('CommentList.vue', () => {
    it('Should render comments items', () => {
        const wrapper = factory({ comments })

        expect(wrapper.findAllComponents(CommentItem)).toHaveLength(2)
    })

    it('Should display the fallback message when comments is empty', () => {
        const wrapper = factory()

        expect(wrapper.find('[data-test=fallback-message]').exists()).toBeTruthy()
    })
})

