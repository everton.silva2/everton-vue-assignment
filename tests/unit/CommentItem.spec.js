import { mount } from "@vue/test-utils"
import CommentItem from '@/components/CommentItem'

const comment = {
    id: '1',
    name: 'name',
    email: 'email',
    body: 'body'
}

const factory = (data) => {
    return mount(CommentItem, {
        propsData: {
            ...data,
            comment
        }
    })
}


describe('CommentItem.vue', () => {
    it('Should render correctly with snapshot', () => {
        const wrapper = factory()

        expect(wrapper.html()).toMatchSnapshot()
    })

    it('Should emit the fill form event when the edit icon is clicked', async () => {
        const wrapper = factory()

        await wrapper.find('[data-test=btn-edit]').trigger('click')

        expect(wrapper.emitted()['fill-form'][0][0]).toBe(comment)
    })

    it('Should emit the delete event when the delete icon is clicked', async () => {
        const wrapper = factory()

        await wrapper.find('[data-test=btn-delete]').trigger('click')

        expect(wrapper.emitted()['delete-comment'][0][0]).toBe(comment.id)
    })
})